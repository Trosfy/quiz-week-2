CREATE TABLE customers(
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255)
);
CREATE TABLE orders(
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
    amount VARCHAR(255),
	customer_id INTEGER, FOREIGN KEY(customer_id) REFERENCES customers(id)
);