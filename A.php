<?php
    function hitung($str)
    {
        $first_num = 0;
        $second_num = 0;
        $operation;
        $i=0;

        while(ord($str[$i])>=ord('0') && ord($str[$i])<=ord('9'))
        {
            $first_num = $first_num*10 + ord($str[$i]) - ord('0');
            $i++;
        }
        $operation = $str[$i++];
        while($i<strlen($str))
        {
            $second_num = $second_num*10 + ord($str[$i]) - ord('0');
            $i++;
        }
        if($operation == '*')
        {
            return $first_num*$second_num;
        }else if($operation == ':')
        {
            return $first_num/$second_num;
        }else if($operation == '+')
        {
            return $first_num+$second_num;
        }else if($operation == '-')
        {
            return $first_num-$second_num;
        }else if($operation == '%')
        {
            return $first_num%$second_num;
        }
    }
    echo hitung("102*2");
    echo hitung("2+3");
    echo hitung("100:25");
?>