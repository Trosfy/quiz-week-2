<?php

    function perolehan_medali($data){
        if(count($data)==0) return "no data";
        $res = array();
        for($i=0;$i<count($data);$i++)
        {
            $flag=0;
            for($j=0;$j<count($res);$j++)
            {
                if($data[$i][0]==$res[$j]['negara'])
                {
                    $flag=1;
                    $res[$j][$data[$i][1]]+=1;
                }
            }
            if($flag==0)
            {
                $res[] = array(
                    "negara" => $data[$i][0],
                    "emas" => 0,
                    "perak" => 0,
                    "perunggu" => 0
                );
                $res[count($res)-1][$data[$i][1]]+=1;
            }
        }
        return $res;
    }
    print_r(perolehan_medali(
        array(
            array('Indonesia','emas'),
            array('India','perak'),
            array('Korea Selatan','emas'),
            array('India','perak'),
            array('India','emas'),
            array('Indonesia','perak'),
            array('Indonesia','emas'),
        )
    ));
?>